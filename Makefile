VERSION=1.0.0

PKG = fakesystemctl
PREFIX ?= /usr/local
DESTDIR ?=
FMODE = -m0644
DMODE = -dm0755

MANF = "fakesystemctl.1"
SCRIPTF = "fakesystemctl"
LICENSEF = "LICENSE"

install_files:
	install $(DMODE) $(DESTDIR)$(PREFIX)/bin	
	install $(DMODE) $(DESTDIR)$(PREFIX)/share/man/man1
	install $(DMODE) $(DESTDIR)$(PREFIX)/share/licenses
	
	install $(FMODE) $(SCRIPTF) $(DESTDIR)$(PREFIX)/bin/systemctl
	install $(FMODE) $(MANF) $(DESTDIR)$(PREFIX)/share/man/man1/systemctl
	gzip $(DESTDIR)$(PREFIX)/share/man/man1/systemctl
	install $(FMODE) $(LICENSEF) $(DESTDIR)$(PREFIX)/share/licenses/systemctl
	
install: install_files

dist:
	git archive --format=tar --prefix=$(PKG)-$(VERSION)/ $(VERSION) | gzip -9 > $(PKG)-$(VERSION).tar.gz
	gpg --detach-sign --use-agent $(PKG)-$(VERSION).tar.gz

.PHONY: install dist
